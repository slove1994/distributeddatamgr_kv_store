#include "cloud/schema_mgr.h"

#include <unordered_set>

#include "cloud_store_types.h"
#include "db_common.h"
#include "db_errno.h"
namespace DistributedDB {
SchemaMgr::SchemaMgr(ICloudSyncStorageInterface *store)
    :store_(store)
{
}

int SchemaMgr::ChkSchema(const TableName &tableName)
{
    if (cloudSchema_ == nullptr) {
        LOGE("Cloud schema has not been set:%d", -E_SCHEMA_NOT_SET);
        return -E_SCHEMA_NOT_SET;
    }
    TableSchema cloudTableSchema;
    int ret = GetTableSchema(tableName, cloudTableSchema);
    if (ret != E_OK) {
        LOGE("Cloud schema does not contain certain table:%d", -E_SCHEMA_MISMATCH);
        return -E_SCHEMA_MISMATCH;
    }
    RelationalSchemaObject localSchema;
    ret = GetLocalSchemaFromMeta(localSchema);
    if (ret != E_OK) {
        return ret;
    }
    TableInfo tableInfo = localSchema.GetTable(tableName);
    if (tableInfo.Empty()) {
        LOGE("Local schema does not contain certain table:%d", -E_SCHEMA_MISMATCH);
        return -E_SCHEMA_MISMATCH;
    }
    std::map<int, FieldName> primaryKeys = tableInfo.GetPrimaryKey();
    FieldInfoMap localFields = tableInfo.GetFields();
    return CompareFieldSchema(primaryKeys, localFields, cloudTableSchema.fields);
}

int SchemaMgr::CompareFieldSchema(std::map<int, FieldName> &primaryKeys, FieldInfoMap &localFields,
    std::vector<Field> &cloudFields)
{
    std::unordered_set<std::string> cloudColNames;
    for (Field &cloudField : cloudFields) {
        if (localFields.find(cloudField.colName) == localFields.end()) {
            LOGE("Column name mismatch between local and cloud schema: %d", -E_SCHEMA_MISMATCH);
            return -E_SCHEMA_MISMATCH;
        }
        FieldInfo &localField = localFields[cloudField.colName];
        if (CompareType(localField, cloudField) == false) {
            LOGE("Type mismatch between local and cloud schema : %d", -E_SCHEMA_MISMATCH);
            return -E_SCHEMA_MISMATCH;
        }
        if (CompareNullable(localField, cloudField) == false) {
            LOGE("The nullable property is mismatched between local and cloud schema : %d", -E_SCHEMA_MISMATCH);
            return -E_SCHEMA_MISMATCH;
        }
        if (CompareIsPrimary(primaryKeys, cloudField) == false) {
            LOGE("The primary key property is mismatched between local and cloud schema : %d", -E_SCHEMA_MISMATCH);
            return -E_SCHEMA_MISMATCH;
        }
        cloudColNames.emplace(cloudField.colName);
    }
    if (primaryKeys.size() > 0) {
        LOGE("Local schema contain extra primary key:%d", -E_SCHEMA_MISMATCH);
        return -E_SCHEMA_MISMATCH;
    }
    for (auto [fieldName, fieldInfo] : localFields) {
        if (fieldInfo.HasDefaultValue() == false && cloudColNames.find(fieldName) == cloudColNames.end()) {
            LOGE("Column from local schema is not within cloud schema but is not nullable : %d", -E_SCHEMA_MISMATCH);
            return -E_SCHEMA_MISMATCH;
        }
    }
    return E_OK;
}

bool SchemaMgr::CompareType(const FieldInfo &localField, const Field &cloudField)
{
    StorageType localType = localField.GetStorageType();
    switch (cloudField.type)
    {
        case TYPE_INDEX<std::monostate>:
            return localType == StorageType::STORAGE_TYPE_NULL;
        case TYPE_INDEX<bool>:
        case TYPE_INDEX<int64_t>:
            return localType == StorageType::STORAGE_TYPE_INTEGER;
        case TYPE_INDEX<double>:
            return localType == StorageType::STORAGE_TYPE_REAL;
        case TYPE_INDEX<std::string>:
            return localType == StorageType::STORAGE_TYPE_TEXT;
        case TYPE_INDEX<Bytes>:
        case TYPE_INDEX<Asset>:
        case TYPE_INDEX<Assets>:
            return localType == StorageType::STORAGE_TYPE_BLOB;
        default:
            return false;
    }
    return false;
}

bool SchemaMgr::CompareNullable(const FieldInfo &localField, const Field &cloudField)
{
    return localField.HasDefaultValue() == cloudField.nullable;
}

bool SchemaMgr::CompareIsPrimary(std::map<int, FieldName> &localPrimaryKeys, const Field &cloudField)
{
    // whether the corresponding field in local schema is primary key
    bool isLocalFieldPrimary = false;
    for (auto kvPair : localPrimaryKeys) {
        if (kvPair.second == cloudField.colName) {
            isLocalFieldPrimary = true;
            localPrimaryKeys.erase(kvPair.first);
            break;
        }
    }
    return isLocalFieldPrimary == cloudField.primary;
}

void SchemaMgr::SetCloudDbSchema(const DataBaseSchema &schema)
{
    cloudSchema_ = std::make_shared<DataBaseSchema>(schema);
}

std::shared_ptr<DataBaseSchema> SchemaMgr::GetCloudDbSchema()
{
    return cloudSchema_;
}

int SchemaMgr::GetTableSchema(const TableName &tableName, TableSchema &retSchema)
{
    if (cloudSchema_ == nullptr) {
        return -E_SCHEMA_NOT_SET;
    }
    for (const TableSchema &tableSchema : cloudSchema_->tables) {
        if (tableSchema.name == tableName) {
            retSchema = tableSchema;
            return E_OK;
        }
    }
    return -E_NOT_FOUND;
}

int SchemaMgr::GetLocalSchemaFromMeta(RelationalSchemaObject &schema)
{
    Key schemaKey;
    DBCommon::StringToVector(DBConstant::RELATIONAL_SCHEMA_KEY, schemaKey);
    Value schemaVal;
    int ret = store_->GetMetaData(schemaKey, schemaVal);
    if (ret != E_OK && ret != -E_NOT_FOUND) {
        LOGE("Get local schema from meta table failed. %d", ret);
        return ret;
    } else if (ret == -E_NOT_FOUND || schemaVal.empty()) {
        LOGW("Local schema info was not found.");
        return -E_NOT_FOUND;
    }
    std::string schemaStr;
    DBCommon::VectorToString(schemaVal, schemaStr);
    ret = schema.ParseFromSchemaString(schemaStr);
    if (ret != E_OK) {
        LOGE("Parse schema string from meta table failed.");
        return ret;
    }
    return E_OK;
}
} // namespace DistributedDB