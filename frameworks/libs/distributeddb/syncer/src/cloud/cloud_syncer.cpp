/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdint.h>
#include <utility>
#include "cloud_syncer.h"
#include "db_errno.h"
#include "icloud_db.h"
#include "kv_store_errno.h"
#include "log_print.h"
#include "runtime_context.h"
#include "securec.h"
#include "strategy_factory.h"
#include "storage_proxy.h"
#include "store_types.h"

namespace DistributedDB {
namespace {
    const TaskId INVALID_TASK_ID = 0u;
    const char *CLOUD_DEVICE = "cloud";
}
CloudSyncer::CloudSyncer(std::shared_ptr<StorageProxy> storageProxy)
    : currentTaskId_(INVALID_TASK_ID),
      storageProxy_(std::move(storageProxy)),
      queuedManualSyncLimit_(DBConstant::QUEUED_SYNC_LIMIT_DEFAULT),
      closed_(false)
{
}

int CloudSyncer::Sync(const std::vector<DeviceID> &devices, SyncMode mode,
    const std::vector<std::string> &tables, const SyncProcessCallback &onProcess,
    int64_t waitTime)
{
    int errCode = CheckParamValid(devices, mode);
    if (errCode != E_OK) {
        return errCode;
    }
    CloudTaskInfo taskInfo = {
        .mode = mode,
        .table = tables,
        .callback = onProcess,
        .timeout = waitTime
    };
    errCode = TryToAddSyncTask(std::move(taskInfo));
    if (errCode != E_OK) {
        return errCode;
    }
    return TriggerSync();
}

int CloudSyncer::SetCloudDB(const std::shared_ptr<ICloudDb> &cloudDB)
{
    cloudDB_.SetCloudDB(cloudDB);
    LOGI("[CloudSyncer] SetCloudDB finish");
    return E_OK;
}

void CloudSyncer::Close()
{
    closed_ = true;
    cloudDB_.Close();
    {
        LOGD("[CloudSyncer] begin wait current task finished");
        std::unique_lock<std::mutex> uniqueLock(contextLock_);
        contextCv_.wait(uniqueLock, [this]() {
            return currentContext_.currentTaskId == INVALID_TASK_ID;
        });
        LOGD("[CloudSyncer] current task has been finished");
    }

    // copy all task from queue
    std::vector<CloudTaskInfo> infoList;
    {
        std::lock_guard<std::mutex> autoLock(queueLock_);
        for (const auto &item: cloudTaskInfos_) {
            infoList.push_back(item.second);
        }
        taskQueue_.clear();
        cloudTaskInfos_.clear();
    }
    // notify all busy
    for (auto &info: infoList) {
        info.status = ProcessStatus::FINISHED;
        info.errCode = -E_BUSY;
        ProcessNotifier notifier;
        notifier.Init(info.table);
        notifier.NotifyProcess(info, {});
    }
}

void CloudSyncer::ProcessNotifier::Init(const std::vector<std::string> &tableName)
{
    std::lock_guard<std::mutex> autoLock(processMutex_);
    syncProcess_.errCode = OK;
    syncProcess_.process = ProcessStatus::PROCESSING;
    for (const auto &table: tableName) {
        TableProcessInfo tableInfo = {
            .process = ProcessStatus::PREPARED
        };
        syncProcess_.tableProcess[table] = tableInfo;
    }
}

void CloudSyncer::ProcessNotifier::UpdateProcess(const CloudSyncer::InnerProcessInfo &process)
{
    std::lock_guard<std::mutex> autoLock(processMutex_);
    syncProcess_.tableProcess[process.tableName].process = process.tableStatus;
    if (process.downLoadInfo.batchIndex != 0u) {
        LOGD("[ProcessNotifier] update download process index: %" PRIu32, process.downLoadInfo.batchIndex);
        syncProcess_.tableProcess[process.tableName].downLoadInfo.batchIndex = process.downLoadInfo.batchIndex;
        syncProcess_.tableProcess[process.tableName].downLoadInfo.total += process.downLoadInfo.total;
        syncProcess_.tableProcess[process.tableName].downLoadInfo.failCount += process.downLoadInfo.failCount;
        syncProcess_.tableProcess[process.tableName].downLoadInfo.successCount += process.downLoadInfo.successCount;
    }
    if (process.upLoadInfo.batchIndex != 0u) {
        LOGD("[ProcessNotifier] update upload process index: %" PRIu32, process.downLoadInfo.batchIndex);
        syncProcess_.tableProcess[process.tableName].upLoadInfo.batchIndex = process.upLoadInfo.batchIndex;
        syncProcess_.tableProcess[process.tableName].upLoadInfo.total += process.upLoadInfo.total;
        syncProcess_.tableProcess[process.tableName].upLoadInfo.failCount += process.upLoadInfo.failCount;
        syncProcess_.tableProcess[process.tableName].upLoadInfo.successCount += process.upLoadInfo.successCount;
    }
}

void CloudSyncer::ProcessNotifier::NotifyProcess(const CloudTaskInfo &taskInfo, const InnerProcessInfo &process)
{
    UpdateProcess(process);
    SyncProcess currentProcess;
    {
        std::lock_guard<std::mutex> autoLock(processMutex_);
        syncProcess_.errCode = TransferDBErrno(taskInfo.errCode);
        syncProcess_.process = taskInfo.status;
        currentProcess = syncProcess_;
    }
    SyncProcessCallback callback = taskInfo.callback;
    if (!callback) {
        return;
    }
    int errCode = RuntimeContext::GetInstance()->ScheduleTask([callback, currentProcess]() {
        callback(currentProcess);
        LOGD("[ProcessNotifier] notify process finish");
    });
    if (errCode != E_OK) {
        LOGW("[ProcessNotifier] schedule notify process failed %d", errCode);
    }
}

int CloudSyncer::TriggerSync()
{
    if (closed_) {
        return -E_BUSY;
    }
    RefObject::IncObjRef(this);
    int errCode = RuntimeContext::GetInstance()->ScheduleTask([this]() {
        DoSyncIfNeed();
        RefObject::DecObjRef(this);
    });
    if (errCode != E_OK) {
        LOGW("[CloudSyncer] schedule sync task failed %d", errCode);
        RefObject::DecObjRef(this);
    }
    return errCode;
}

void CloudSyncer::DoSyncIfNeed()
{
    if (closed_) {
        return;
    }
    // get taskId from queue
    TaskId triggerTaskId;
    {
        std::lock_guard<std::mutex> autoLock(queueLock_);
        if (taskQueue_.empty()) {
            return;
        }
        triggerTaskId = taskQueue_.front();
    }
    // pop taskId in queue
    if (PrepareSync(triggerTaskId) != E_OK) {
        return;
    }
    // do sync logic
    int errCode = DoSync(triggerTaskId);
    // finished after sync
    DoFinished(triggerTaskId, errCode, {});
    // do next task async
    (void)TriggerSync();
}

int CloudSyncer::DoSync(TaskId taskId)
{
    CloudTaskInfo taskInfo;
    {
        std::lock_guard<std::mutex> autoLock(queueLock_);
        taskInfo = cloudTaskInfos_[taskId];
    }
    int errCode = E_OK;
    for (const auto &table: taskInfo.table) {
        if (closed_) {
            LOGD("[CloudSyncer] syncer is closed, abort sync");
            return -E_BUSY;
        }
        {
            std::lock_guard<std::mutex> autoLock(contextLock_);
            currentContext_.tableName = table;
        }
        errCode = DoDownload(taskId);
        if (errCode != E_OK) {
            LOGD("[CloudSyncer] download failed %d", errCode);
            return errCode;
        }
    }
    for (const auto &table: taskInfo.table) {
        if (closed_) {
            LOGD("[CloudSyncer] syncer is closed, abort sync");
            return -E_BUSY;
        }
        {
            std::lock_guard<std::mutex> autoLock(contextLock_);
            currentContext_.tableName = table;
        }
        errCode = DoUpload(taskId);
        if (errCode != E_OK) {
            LOGD("[CloudSyncer] upload failed %d", errCode);
            return errCode;
        }
    }
    return E_OK;
}

void CloudSyncer::DoFinished(TaskId taskId, int errCode, const InnerProcessInfo &processInfo)
{
    std::shared_ptr<ProcessNotifier> notifier = nullptr;
    {
        // check current task is running or not
        std::lock_guard<std::mutex> autoLock(contextLock_);
        if (currentContext_.currentTaskId != taskId) { // should not happen
            LOGW("[CloudSyncer] task %" PRIu32 " not exist in context!", taskId);
            return;
        }
        currentContext_.currentTaskId = INVALID_TASK_ID;
        notifier = currentContext_.notifier;
        currentContext_.notifier = nullptr;
        currentContext_.strategy = nullptr;
        currentContext_.tableName.clear();
    }
    CloudTaskInfo info;
    {
        std::lock_guard<std::mutex> autoLock(queueLock_);
        if (cloudTaskInfos_.find(taskId) == cloudTaskInfos_.end()) { // should not happen
            LOGW("[CloudSyncer] task %" PRIu32 " has been finished!", taskId);
            contextCv_.notify_one();
            return;
        }
        info = std::move(cloudTaskInfos_[taskId]);
        cloudTaskInfos_.erase(taskId);
    }
    contextCv_.notify_one();
    info.errCode = errCode;
    info.status = ProcessStatus::FINISHED;
    if (notifier != nullptr) {
        notifier->NotifyProcess(info, processInfo);
    }
}

int CloudSyncer::SaveData(const TableName &tableName, DownloadData &downloadData, Info &downloadInfo)
{
    // Update download btach Info
    downloadInfo.batchIndex += 1;
    downloadInfo.total += downloadData.data.size();
    // Tag every datum in data set
    int ret = E_OK;
    for (size_t i = 0; i < downloadData.data.size(); i++) {
        LogInfo localLogInfo;
        bool isExist = true;
        ret = storageProxy_->GetLogInfoByPrimaryKeyOrGid(tableName, downloadData.data[i], localLogInfo);
        if (ret == -E_NOT_FOUND) {
            isExist = false;
        } else if (ret != E_OK) {
            LOGE("[CloudSyncer] Cannot get cloud water level from cloud meta data: %d.", ret);
            return ret;
        }
        // Get cloudLogInfo from cloud data
        LogInfo cloudLogInfo = {
            .timestamp = (Timestamp)std::get<int64_t>(downloadData.data[i][CloudDbConstant::MODIFY_FIELD]),
            .wTimestamp = (Timestamp)std::get<int64_t>(downloadData.data[i][CloudDbConstant::CREATE_FIELD]),
            .flag = (std::get<bool>(downloadData.data[i][CloudDbConstant::DELETE_FIELD])) == true ? 1u : 0u,
            .cloudGid = std::get<std::string>(downloadData.data[i][CloudDbConstant::GID_FIELD]),
        };
        // Tag datum and get opType
        downloadData.opType[i] = currentContext_.strategy->TagSyncDataStatus(isExist, localLogInfo, cloudLogInfo);
    }
    // save the data to the database by batch
    // TODO: the interface needs to return successCnt
    int successCnt = 0;
    ret = storageProxy_->PutCloudSyncData(tableName, downloadData);
    if (ret != E_OK) {
        LOGE("[CloudSyncer] Cannot save the data to databse with error code: %d.", ret);
        return ret;
    }
    // Update downloadInfo
    downloadInfo.successCount = successCnt;
    downloadInfo.failCount = downloadData.data.size() - successCnt;
    return E_OK;
}

int CloudSyncer::PreCheck(CloudSyncer::TaskId &taskId, std::string &tableName)
{
    // Check Input and Context Validity
    if (closed_) {
        LOGE("DB is closed.");
        return E_INVALID_DB;
    }
    {
        std::lock_guard<std::mutex> autoLock(queueLock_);
        if (cloudTaskInfos_.find(taskId) == cloudTaskInfos_.end()) {
            LOGE("[CloudSyncer] Cloud Task Info does not exist Task Id: , %" PRIu64 ".", taskId);
            return -E_INVALID_ARGS;
        }
    }
    int ret = GetCurrentTableName(tableName);
    if (ret != E_OK) {
        LOGE("[CloudSyncer] Invalid table name for syncing: %d", ret);
        return ret;
    }
    ret = storageProxy_->CheckSchema(tableName);
    if (ret != E_OK) {
        LOGE("A schema error occurred on the table to be synced, %d", ret);
        return ret;
    }
    return E_OK;
}

int CloudSyncer::DoDownload(CloudSyncer::TaskId taskId)
{
    TableName tableName;
    int ret = PreCheck(taskId, tableName);
    if (ret != E_OK) {
        return ret;
    }
    InnerProcessInfo innerProcessInfo;
    DownloadData downloadData;
    bool reachEnd = false;
    // Query data by batch until reaching end and not more data need to be download
    while (!reachEnd) {
        // Get cloud data after cloud water mark
        ret = QueryCloudData(tableName, downloadData.data);
        if (ret == -E_REACH_END) {
            reachEnd = true;
        } else if (ret != E_OK) {
            return ret;
        }
        ret = storageProxy_->StartTransaction(TransactType::DEFERRED);
        if (ret != E_OK) {
            LOGE("[CloudSyncer] Cannot start a transaction: %d.", ret);
            return ret;
        }
        ret = SaveData(tableName, downloadData, innerProcessInfo.downLoadInfo);
        if (ret != E_OK) {
            {
                std::lock_guard<std::mutex> autoLock(contextLock_);
                currentContext_.notifier->UpdateProcess(innerProcessInfo);
            }
            return storageProxy_->Rollback();
        }
        ret = storageProxy_->Commit();
        if (ret != E_OK) {
            LOGE("[CloudSyncer] Cannot commit a transaction: %d.", ret);
            return ret;
        }
        {
            std::lock_guard<std::mutex> autoLock(contextLock_);
            currentContext_.notifier->NotifyProcess(cloudTaskInfos_[taskId], innerProcessInfo);
        }
        // use the cursor of the last datum in data set to update cloud water mark 
        CloudWaterMark newCloudWaterMark;
        VBucket lastData = downloadData.data.back();
        newCloudWaterMark = std::get<std::string>(lastData[CloudDbConstant::CURSOR_FIELD]);
        ret = storageProxy_->PutCloudWaterMark(tableName, newCloudWaterMark);
        if (ret != E_OK) {
            LOGE("[CloudSyncer] Cannot set cloud water mark while downloading, %d.", ret);
            return ret;
        }
    }
    return E_OK;
}

int CloudSyncer::PreCheckUpload(CloudSyncer::TaskId &taskId, std::string &tableName)
{
    int ret = PreCheck(taskId, tableName);
    if (ret != E_OK) {
        return ret;
    }
    if ((cloudTaskInfos_[taskId].mode < SYNC_MODE_CLOUD_MERGE) || (cloudTaskInfos_[taskId].mode > SYNC_MODE_CLOUD_FORCE_PUSH)) {
        LOGE("[CloudSyncer] Upload failed, invalid sync mode: %d.", static_cast<int>(cloudTaskInfos_[taskId].mode));
        return -E_INVALID_ARGS;
    }
    return E_OK;
}

bool CloudSyncer:: CheckCloudSyncDataEmpty(CloudSyncData &uploadData)
{
    return uploadData.insData.extend.empty() && uploadData.insData.record.empty() &&
           uploadData.updData.extend.empty() && uploadData.updData.record.empty() &&
           uploadData.delData.extend.empty() && uploadData.delData.record.empty();
}

int CloudSyncer::DoBatchUpload(CloudSyncData &uploadData, const int64_t &count, const TaskId taskId, InnerProcessInfo &innerProcessInfo)
{
    int errCode = E_OK;
    Info insertInfo;
    Info updateInfo;
    Info deleteInfo;
    errCode = cloudDB_.BatchInsert(uploadData.tableName, uploadData.insData.record, uploadData.insData.extend, insertInfo);
    innerProcessInfo.upLoadInfo.total += insertInfo.total;
    if (errCode != E_OK) {
        innerProcessInfo.upLoadInfo.failCount += insertInfo.failCount;
        // if the upload is partilly successed, call UpdateProcess;
        {
            std::lock_guard<std::mutex> autoLock(contextLock_);
            currentContext_.notifier->UpdateProcess(innerProcessInfo);
        }
        return errCode;
    } else {
        innerProcessInfo.upLoadInfo.successCount += insertInfo.successCount;
        // we need to fill back gid after insert data to cloud.
        int ret = storageProxy_->FillCloudGid(uploadData);
        if (ret != E_OK) {
            LOGE("[CloudSyncer] Failed to fill back gid when doing upload, %d.", ret);
            return ret;
        }
    }

    errCode = cloudDB_.BatchUpdate(uploadData.tableName, uploadData.updData.record, uploadData.updData.extend, updateInfo);
    innerProcessInfo.upLoadInfo.total += updateInfo.total;
    if (errCode != E_OK) {
        innerProcessInfo.upLoadInfo.failCount += updateInfo.failCount;
        {
            std::lock_guard<std::mutex> autoLock(contextLock_);
            currentContext_.notifier->UpdateProcess(innerProcessInfo);
        }
        return errCode;
    } else {
        innerProcessInfo.upLoadInfo.successCount += updateInfo.successCount;
    }

    errCode = cloudDB_.BatchDelete(uploadData.tableName, uploadData.delData.record, uploadData.delData.extend, deleteInfo);
    innerProcessInfo.upLoadInfo.total += deleteInfo.total;
    if (errCode != E_OK) {
        innerProcessInfo.upLoadInfo.failCount += deleteInfo.failCount;
        {
            std::lock_guard<std::mutex> autoLock(contextLock_);
            currentContext_.notifier->UpdateProcess(innerProcessInfo);
        }
        return errCode;
    } else {
        innerProcessInfo.upLoadInfo.successCount += deleteInfo.successCount;
    }
    
    // if the upload process is totally successed, call NotifyProcess
    {
        std::lock_guard<std::mutex> autoLock(contextLock_);
        currentContext_.notifier->NotifyProcess(cloudTaskInfos_[taskId], innerProcessInfo);
    }

    // // update local water mark
    // LocalWaterMark localMark;
    // int ret = GetLatestLocalWatrMark(uploadData, localMark, count, taskId);
    // if (ret != E_OK) {
    //     LOGE("[CloudSyncer] Failed to get new local water mark in Cloud Sync Data, %d.", ret);
    //     return ret;
    // }

    // ret = storageProxy_->PutLocalWaterMark(uploadData.tableName, localMark);
    // if (ret != E_OK) {
    //     LOGE("[CloudSyncer] Failed to set local water mark when doing upload, %d.", ret);
    //     return ret;
    // }

    // The cloud water mark cannot be updated here, because the cloud api doesn't return cursor here.

    return E_OK;
}

int CloudSyncer::DoUpload(CloudSyncer::TaskId taskId)
{
    storageProxy_->StartTransaction();
    int errCode = DoUploadInner(taskId);
    if (errCode == E_OK) {
        storageProxy_->Commit();
    } else {
        storageProxy_->Rollback();
    }
    return errCode;
}

int CloudSyncer::DoUploadInner(CloudSyncer::TaskId taskId)
{
    std::string tableName;
    int ret = PreCheckUpload(taskId, tableName);
    if (ret != E_OK) {
        LOGE("[CloudSyncer] Doing upload sync pre check failed, %d.", ret);
        return ret;
    }

    LocalWaterMark localMark;
    ret = storageProxy_->GetLocalWaterMark(tableName, localMark);
    if (ret != E_OK) {
        LOGE("[CloudSyncer] Failed to get local water mark when upload, %d.", ret);
        return ret;
    }

    int64_t count = 0;
    ret = storageProxy_->GetUploadCount(tableName, localMark, count);
    if (ret != E_OK) {
        // GetUploadCount will return E_OK when upload count is zero.
        LOGE("[CloudSyncer] Failed to get Upload Data Count, %d.", ret);
        return ret;
    }
    if (count == 0) {
        LOGI("[CloudSyncer] There is no need to doing upload, as the upload data count is zero.");
        return E_OK;
    }

    ContinueToken continueStmtToken = NULL;
    CloudSyncData uploadData(tableName);

    ret = storageProxy_->GetCloudData(tableName, localMark, continueStmtToken, uploadData);
    if ((ret != E_OK) && (ret != -E_UNFINISHED)) {
        LOGE("[CloudSyncer] Failed to get cloud data when upload, %d.", ret);
        return ret;
    }

    InnerProcessInfo innerProcessInfo;
    innerProcessInfo.upLoadInfo.total = count;
    uint32_t batchIndex = 0;
    
    while (!CheckCloudSyncDataEmpty(uploadData)) {
        bool getDataUnfinished = ret == -E_UNFINISHED ? true : false;
        if (closed_) {
            LOGE("[CloudSyncer] DB is closed.");
            if (getDataUnfinished) {
                storageProxy_->ReleaseContinueToken(continueStmtToken);
            }
            return -E_INVALID_DB;
        }
        ret = CheckCloudSyncDataValid(uploadData, tableName, count, taskId);
        if (ret != E_OK) {
            LOGE("[CloudSyncer] Invalid Cloud Sync Data of Upload, %d.", ret);
            if (getDataUnfinished) {
                storageProxy_->ReleaseContinueToken(continueStmtToken);
            }
            return ret;
        }

        innerProcessInfo.upLoadInfo.batchIndex = ++batchIndex;

        // update local water mark
        LocalWaterMark localMark;
        int ret = GetLatestLocalWatrMark(uploadData, localMark, count, taskId);
        if (ret != E_OK) {
            LOGE("[CloudSyncer] Failed to get new local water mark in Cloud Sync Data, %d.", ret);
            if (getDataUnfinished) {
                storageProxy_->ReleaseContinueToken(continueStmtToken);
            }
            return ret;
        }

        ret = DoBatchUpload(uploadData, count, taskId, innerProcessInfo);
        if (ret != E_OK) {
            LOGE("[CloudSyncer] Failed to do upload, %d", ret);
            if (getDataUnfinished) {
                storageProxy_->ReleaseContinueToken(continueStmtToken);
            }
            return ret;
        }
        // if batch upload successed, update local water mark
        ret = storageProxy_->PutLocalWaterMark(uploadData.tableName, localMark);
        if (ret != E_OK) {
            LOGE("[CloudSyncer] Failed to set local water mark when doing upload, %d.", ret);
            if (getDataUnfinished) {
                storageProxy_->ReleaseContinueToken(continueStmtToken);
            }
            return ret;
        }

        // After each batch upload successed, call NotifyProcess.
        {
            std::lock_guard<std::mutex> autoLock(contextLock_);
            currentContext_.notifier->NotifyProcess(cloudTaskInfos_[taskId], innerProcessInfo);
        }

        ClearCloudSyncData(uploadData);
        
        ret = storageProxy_->GetCloudDataNext(continueStmtToken, uploadData);
        if ((ret != E_OK) && (ret != -E_UNFINISHED)) {
            LOGE("[CloudSyncer] Failed to get cloud data next when doing upload, %d.", ret);
            return ret;
        }
    }
    return E_OK;
}

int CloudSyncer::CheckDataValid(const DownloadData &data, std::string &cursor)
{
    return E_OK;
}

int CloudSyncer::QueryCloudData(const std::string &tableName, std::vector<VBucket> &data)
{
    CloudWaterMark cloudWaterMark;
    int ret = storageProxy_->GetCloudWaterMark(tableName, cloudWaterMark);
    if (ret != E_OK) {
        LOGE("[CloudSyncer] Cannot get cloud water level from cloud meta data: %d.", ret);
        return ret;
    }
    VBucket extend = {
        {CloudDbConstant::CURSOR_FIELD, cloudWaterMark}
    };
    ret = cloudDB_.Query(tableName, extend, data);
    if (ret == -E_REACH_END) {
        LOGI("[CloudSyncer] Download data from cloud database usuccess and no more data need to be downloaded");
        return -E_REACH_END;
    }
    if (ret == OK) {
        LOGI("[CloudSyncer] Download data from cloud database usuccess but still has data to download");
        return E_OK;
    }
    LOGE("[CloudSyncer] Download data from cloud database unsuccess");
    return -E_CLOUD_ERROR;
}

int CloudSyncer::CheckParamValid(const std::vector<DeviceID> &devices, SyncMode mode)
{
    if (devices.empty()) {
        LOGE("[CloudSyncer] devices is empty");
        return -E_INVALID_ARGS;
    }
    if (devices.size() > 1) {
        LOGE("[CloudSyncer] too much devices");
        return -E_INVALID_ARGS;
    }
    for (const auto &dev: devices) {
        if (dev != CLOUD_DEVICE) {
            LOGE("[CloudSyncer] invalid devices %s", STR_MASK(dev));
            return -E_INVALID_ARGS;
        }
    }
    if (mode < SyncMode::SYNC_MODE_CLOUD_MERGE || mode > SyncMode::SYNC_MODE_CLOUD_FORCE_PULL) {
        LOGE("[CloudSyncer] invalid mode %d", static_cast<int>(mode));
        return -E_INVALID_ARGS;
    }
    return E_OK;
}

bool CloudSyncer::CheckTaskIdValid(TaskId taskId)
{
    {
        std::lock_guard<std::mutex> autoLock(queueLock_);
        if (cloudTaskInfos_.find(taskId) == cloudTaskInfos_.end()) {
            return false;
        }
    }
    std::lock_guard<std::mutex> autoLock(contextLock_);
    return currentContext_.currentTaskId == taskId;
}

int CloudSyncer::GetCurrentTableName(std::string &tableName)
{
    std::lock_guard<std::mutex> autoLock(contextLock_);
    if (currentContext_.tableName.empty()) {
        return -E_BUSY;
    }
    tableName = currentContext_.tableName;
    return E_OK;
}

int CloudSyncer::TryToAddSyncTask(CloudTaskInfo &&taskInfo)
{
    if (closed_) {
        LOGW("[CloudSyncer] syncer is closed, should not sync now");
        return -E_BUSY;
    }
    std::lock_guard<std::mutex> autoLock(queueLock_);
    int errCode = CheckQueueSizeWithNoLock();
    if (errCode != E_OK) {
        return errCode;
    }
    do {
        currentTaskId_++;
    } while (currentTaskId_ == 0);
    taskInfo.taskId = currentTaskId_;
    taskQueue_.push_back(currentTaskId_);
    cloudTaskInfos_[currentTaskId_] = taskInfo;
    LOGI("[CloudSyncer] Add task ok, taskId %" PRIu32, cloudTaskInfos_[currentTaskId_].taskId);
    return E_OK;
}

int CloudSyncer::CheckQueueSizeWithNoLock()
{
    int32_t limit = queuedManualSyncLimit_;
    if (taskQueue_.size() >= static_cast<size_t>(limit)) {
        LOGW("[CloudSyncer] too much sync task");
        return -E_BUSY;
    }
    return E_OK;
}

int CloudSyncer::PrepareSync(TaskId taskId)
{
    std::vector<std::string> tableNames;
    SyncMode mode;
    {
        std::lock_guard<std::mutex> autoLock(queueLock_);
        if (closed_ || cloudTaskInfos_.find(taskId) == cloudTaskInfos_.end()) {
            LOGW("[CloudSyncer] Abort sync because syncer is closed");
            return -E_BUSY;
        }
        tableNames = cloudTaskInfos_[taskId].table;
        mode = cloudTaskInfos_[taskId].mode;
    }
    {
        // check current task is running or not
        std::lock_guard<std::mutex> autoLock(contextLock_);
        if (closed_ || currentContext_.currentTaskId != INVALID_TASK_ID) {
            LOGW("[CloudSyncer] Abort sync because syncer is closed or another task is running");
            return -E_BUSY;
        }
        currentContext_.currentTaskId = taskId;
        currentContext_.notifier = std::make_shared<ProcessNotifier>();
        currentContext_.strategy = StrategyFactory::BuildSyncStrategy(mode);
        currentContext_.notifier->Init(tableNames);
    }
    // remove task id from queue
    std::lock_guard<std::mutex> autoLock(queueLock_);
    if (!taskQueue_.empty()) {
        taskQueue_.pop_front();
    }
    cloudTaskInfos_[taskId].status = ProcessStatus::PROCESSING;
    return E_OK;
}

int CloudSyncer::CheckCloudSyncDataValid(CloudSyncData uploadData, const std::string &tableName, const int64_t &count, TaskId &taskId)
{
    long unsigned int insRecordLen = uploadData.insData.record.size();
    long unsigned int insExtendLen = uploadData.insData.extend.size();
    long unsigned int updRecordLen = uploadData.updData.record.size();
    long unsigned int updExtendLen = uploadData.updData.extend.size();
    long unsigned int delRecordLen = uploadData.delData.record.size();
    long unsigned int delExtendLen = uploadData.delData.extend.size();

    bool SyncDataValid = uploadData.tableName == tableName &&
            (insRecordLen > 0 && insExtendLen > 0 && insRecordLen == insExtendLen) ||
            (updRecordLen > 0 && updExtendLen > 0 && updRecordLen == updExtendLen) ||
            (delRecordLen > 0 && delExtendLen > 0 && delRecordLen == delExtendLen);

    if (!SyncDataValid) {
        LOGE("upload data is empty but upload count is not zero or upload table name is not the same as table name of sync data.");
        return -E_INVALID_CLOUD_SYNC_DATA;
    }
    int64_t SyncDataCount = uploadData.insData.record.size() + uploadData.updData.record.size() + uploadData.delData.record.size();

    if (SyncDataCount > count) {
        LOGE("Size of a batch of sync data is greater than upload data size.");
        return -E_INVALID_CLOUD_SYNC_DATA;
    }

    return E_OK;
}

// After doing a batch upload, we need to use CloudSyncData's maximum timestamp to update the water mark;
int CloudSyncer::GetLatestLocalWatrMark(CloudSyncData &uploadData, LocalWaterMark &WaterMark, const int64_t &count, TaskId taskId)
{
    if (!CheckCloudSyncDataValid(uploadData, uploadData.tableName, count, taskId)) {
        LOGE("[CloudSyncer] Invalid Sync Data when get local water mark.", -E_INVALID_CLOUD_SYNC_DATA);
        return -E_INVALID_CLOUD_SYNC_DATA;
    }

    if (uploadData.insData.extend.size() > 0) {
        if (uploadData.insData.record.size() != uploadData.insData.extend.size()) {
            LOGE("[CloudSyncer] Inconsistent size of inserted data.", -E_INVALID_CLOUD_SYNC_DATA);
            return -E_INVALID_CLOUD_SYNC_DATA;
        }
        for(auto extendData : uploadData.insData.extend) {
            WaterMark = std::max(int64_t(WaterMark), std::get<int64_t>(extendData[CloudDbConstant::MODIFY_FIELD]));
        }
    }

    if (uploadData.updData.extend.size() > 0) {
        if (uploadData.updData.record.size() != uploadData.updData.extend.size()) {
            LOGE("[CloudSyncer] Inconsistent size of updated data, %d.", -E_INVALID_CLOUD_SYNC_DATA);
            return -E_INVALID_CLOUD_SYNC_DATA;
        }
        for(auto extendData : uploadData.updData.extend) {
            WaterMark = std::max(int64_t(WaterMark), std::get<int64_t>(extendData[CloudDbConstant::MODIFY_FIELD]));
        }
    }

    if (uploadData.delData.extend.size() > 0) {
        if (uploadData.delData.record.size() != uploadData.delData.extend.size()) {
            LOGE("[CloudSyncer] Inconsistent size of deleted data, %d.", -E_INVALID_CLOUD_SYNC_DATA);
            return -E_INVALID_CLOUD_SYNC_DATA;
        }
        for(auto extendData : uploadData.delData.extend) {
            WaterMark = std::max(int64_t(WaterMark), std::get<int64_t>(extendData[CloudDbConstant::MODIFY_FIELD]));
        }
    }
    return E_OK;
}

void CloudSyncer::ClearCloudSyncData(CloudSyncData &uploadData) {
    std::vector<VBucket>().swap(uploadData.insData.record);
    std::vector<VBucket>().swap(uploadData.insData.extend);
    std::vector<VBucket>().swap(uploadData.updData.record);
    std::vector<VBucket>().swap(uploadData.updData.extend);
    std::vector<VBucket>().swap(uploadData.delData.record);
    std::vector<VBucket>().swap(uploadData.delData.extend);
}

} // namespace DistributedDB
