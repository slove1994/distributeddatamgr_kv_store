/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CLOUD_SYNCER_H
#define CLOUD_SYNCER_H
#include <atomic>
#include <condition_variable>
#include <mutex>
#include "cloud_db_proxy.h"
#include "cloud_store_types.h"
#include "cloud_sync_strategy.h"
#include "db_common.h"
#include "data_transformer.h"
#include "icloud_db.h"
#include "ref_object.h"
#include "storage_proxy.h"

namespace DistributedDB {
using SyncProcessCallback = std::function<void(SyncProcess)>;
class CloudSyncer : public RefObject{
public:
    explicit CloudSyncer(std::shared_ptr<StorageProxy> storageProxy);
    ~CloudSyncer() override = default;
    DISABLE_COPY_ASSIGN_MOVE(CloudSyncer);

    int Sync(const std::vector<DeviceID> &devices, SyncMode mode, const std::vector<std::string> &tables,
        const SyncProcessCallback &callback, int64_t waitTime);

    int SetCloudDB(const std::shared_ptr<ICloudDb> &cloudDB);

    void Close();
protected:
    using TaskId = uint64_t;
    struct CloudTaskInfo {
        SyncMode mode = SyncMode::SYNC_MODE_PUSH_ONLY;
        ProcessStatus status = ProcessStatus::PREPARED;
        int errCode = 0;
        TaskId taskId = 0u;
        std::vector<std::string> table;
        SyncProcessCallback callback;
        int64_t timeout;
    };
    struct InnerProcessInfo {
        std::string tableName;
        ProcessStatus tableStatus = ProcessStatus::PREPARED;
        Info downLoadInfo;
        Info upLoadInfo;
    };
    class ProcessNotifier {
    public:
        void Init(const std::vector<std::string> &tableName);

        void UpdateProcess(const InnerProcessInfo &process);

        void NotifyProcess(const CloudTaskInfo &taskInfo, const InnerProcessInfo &process);
    protected:
        std::mutex processMutex_;
        SyncProcess syncProcess_;
    };
    struct TaskContext {
        TaskId currentTaskId = 0u;
        std::string tableName;
        std::shared_ptr<ProcessNotifier> notifier;
        std::shared_ptr<CloudSyncStrategy> strategy;
    };

    int TriggerSync();

    void DoSyncIfNeed();

    int DoSync(TaskId taskId);

    void DoFinished(TaskId taskId, int errCode, const InnerProcessInfo &processInfo);

    int DoDownload(TaskId taskId);

    int PreCheckUpload(TaskId &taskId, std::string &tableName);

    int PreCheck(TaskId &taskId, std::string &tableName);

    int DoBatchUpload(CloudSyncData &uploadData, const int64_t &count, const TaskId taskId, InnerProcessInfo &innerProcessInfo);
    
    int CheckCloudSyncDataValid(CloudSyncData uploadData, const std::string &tableName, const int64_t &count, TaskId &taskId);

    bool CheckCloudSyncDataEmpty(CloudSyncData &uploadData);

    int GetLatestLocalWatrMark(CloudSyncData &uploadData, LocalWaterMark &WaterMark, const int64_t &count, TaskId taskId);

    void ClearCloudSyncData(CloudSyncData &uploadData);

    int DoUpload(TaskId taskId);

    int DoUploadInner(TaskId taskId);

    int CheckDataValid(const DownloadData &data, std::string &cursor);

    int QueryCloudData(const std::string &tableName, std::vector<VBucket> &data);

    bool CheckTaskIdValid(TaskId taskId);

    int GetCurrentTableName(std::string &tableName);

    int TryToAddSyncTask(CloudTaskInfo &&taskInfo);

    int CheckQueueSizeWithNoLock();

    int PrepareSync(TaskId taskId);

    int SaveData(const TableName &tablename, DownloadData &downloadData, Info &downloadInfo);

    static int CheckParamValid(const std::vector<DeviceID> &devices, SyncMode mode);

    std::mutex queueLock_;
    std::atomic<TaskId> currentTaskId_;
    std::list<TaskId> taskQueue_;
    std::map<TaskId, CloudTaskInfo> cloudTaskInfos_;

    std::mutex contextLock_;
    TaskContext currentContext_;
    std::condition_variable contextCv_;

    CloudDBProxy cloudDB_;

    std::shared_ptr<StorageProxy> storageProxy_;
    std::atomic<int32_t> queuedManualSyncLimit_;

    std::atomic<bool> closed_;
};
}
#endif // CLOUD_SYNCER_H
