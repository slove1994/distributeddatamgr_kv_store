/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include "cloud_store_types.h"
#include "db_common.h"
#include "distributeddb_data_generate_unit_test.h"
#include "distributeddb_tools_unit_test.h"
#include "icloud_sync_storage_interface.h"
#include "relational_store_instance.h"
#include "relational_store_manager.h"
#include "relational_sync_able_storage.h"
#include "sqlite_relational_store.h"
#include "storage_proxy.h"


using namespace testing::ext;
using namespace  DistributedDB;
using namespace  DistributedDBUnitTest;

namespace {
    constexpr const char *DB_SUFFIX = ".db";
    constexpr const char *STORE_ID = "Relational_Store_ID";
    std::string g_dbDir;
    std::string g_testDir;
    std::string g_tableName = "sync_data";
    std::string g_storePath;
    DistributedDB::RelationalStoreManager g_mgr(APP_ID, USER_ID);
    IRelationalStore *g_store = nullptr;
    RelationalStoreDelegate *g_delegate = nullptr;
    ICloudSyncStorageInterface *g_cloudStore = nullptr;

    class DistributedDBSaveCloudDataTest : public testing::Test {
    public:
        static void SetUpTestCase(void);
        static void TearDownTestCase(void);
        void SetUp() override;
        void TearDown() override;
    };

    void CreatDB()
    {
        sqlite3 *db = RelationalTestUtils::CreateDataBase(g_dbDir + STORE_ID + DB_SUFFIX);
        EXPECT_NE(db, nullptr);
        EXPECT_EQ(RelationalTestUtils::ExecSql(db, "PRAGMA journal_mode=WAL;"), SQLITE_OK);
        EXPECT_EQ(sqlite3_close_v2(db), E_OK);
    }

    void SetCloudSchema()
    {
        Field field1 = { "id", TYPE_INDEX<int64_t>, true, true };
        Field field2 = { "name", TYPE_INDEX<std::string>, false, true };
        TableSchema tableSchema = { g_tableName, { field1, field2} };
        DataBaseSchema dbSchema;
        dbSchema.tables = { tableSchema };
        g_cloudStore->SetCloudDbSchema(dbSchema);
    }

    void PrepareDataBase(const std::string &tableName, bool primaryKeyIsRowId)
    {
        /**
         * @tc.steps:step1. create table.
         * @tc.expected: step1. return ok.
         */
        sqlite3 *db = RelationalTestUtils::CreateDataBase(g_dbDir + STORE_ID + DB_SUFFIX);
        EXPECT_NE(db, nullptr);
        std::string sql;
        if (primaryKeyIsRowId) {
            sql = "create table " + tableName + "(id int primary key, name TEXT);";
        } else {
            sql = "create table " + tableName + "(id int, name TEXT);";
        }
        EXPECT_EQ(RelationalTestUtils::ExecSql(db, sql), E_OK);


        /**
         * @tc.steps:step2. create distributed table with CLOUD_COOPERATION mode.
         * @tc.expected: step2. return ok.
         */
        EXPECT_EQ(g_delegate->CreateDistributedTable(tableName, DistributedDB::CLOUD_COOPERATION), OK);

        /**
         * @tc.steps:step3. insert one row.
         * @tc.expected: step3. return ok.
         */
        sql = "insert into " + tableName + " values(1, 'ab');";
        EXPECT_EQ(RelationalTestUtils::ExecSql(db, sql), E_OK);
        EXPECT_EQ(sqlite3_close_v2(db), E_OK);
    }

    void InitStoreProp(const std::string &storePath, const std::string &appId, const std::string &userId,
        RelationalDBProperties &properties)
    {
        properties.SetStringProp(RelationalDBProperties::DATA_DIR, storePath);
        properties.SetStringProp(RelationalDBProperties::APP_ID, appId);
        properties.SetStringProp(RelationalDBProperties::USER_ID, userId);
        properties.SetStringProp(RelationalDBProperties::STORE_ID, STORE_ID);
        std::string identifier = userId + "-" + appId + "-" + STORE_ID;
        std::string hashIdentifier = DBCommon::TransferHashString(identifier);
        properties.SetStringProp(RelationalDBProperties::IDENTIFIER_DATA, hashIdentifier);
    }

    const RelationalSyncAbleStorage *GetRelationalStore()
    {
        RelationalDBProperties properties;
        InitStoreProp(g_storePath, APP_ID, USER_ID, properties);
        int errCode = E_OK;
        g_store = RelationalStoreInstance::GetDataBase(properties, errCode);
        if (g_store == nullptr) {
            LOGE("Get db failed:%d", errCode);
            return nullptr;
        }
        return static_cast<SQLiteRelationalStore *>(g_store)->GetStorageEngine();
    }

    void DistributedDBSaveCloudDataTest::SetUpTestCase(void)
    {
        DistributedDBToolsUnitTest::TestDirInit(g_testDir);
        LOGD("Test dir is %s", g_testDir.c_str());
        g_dbDir = g_testDir + "/";
        g_storePath = g_dbDir + STORE_ID + DB_SUFFIX;
        DistributedDBToolsUnitTest::RemoveTestDbFiles(g_testDir);
    }

    void DistributedDBSaveCloudDataTest::TearDownTestCase(void)
    {
    }

    void DistributedDBSaveCloudDataTest::SetUp()
    {
        CreatDB();
        DBStatus status = g_mgr.OpenStore(g_dbDir + STORE_ID + DB_SUFFIX, STORE_ID, {}, g_delegate);
        EXPECT_EQ(status, OK);
        ASSERT_NE(g_delegate, nullptr);
        g_cloudStore = (ICloudSyncStorageInterface *) GetRelationalStore();
        ASSERT_NE(g_cloudStore, nullptr);
        SetCloudSchema();
    }

    void DistributedDBSaveCloudDataTest::TearDown()
    {
        DistributedDBToolsUnitTest::RemoveTestDbFiles(g_testDir);
    }

    std::shared_ptr<StorageProxy> GetStorageProxy(ICloudSyncStorageInterface *store)
    {
        return StorageProxy::GetCloudDb(store);
    }

    /**
     * @tc.name: GetLogInfoByPrimaryKeyOrGidTest001
     * @tc.desc: Test get_raw_sys_time has been registered in sqlite
     * @tc.type: FUNC
     * @tc.require:
     * @tc.author: zhangshijie
     */
    HWTEST_F(DistributedDBSaveCloudDataTest, GetLogInfoByPrimaryKeyOrGidTest001, TestSize.Level0)
    {
        /**
         * @tc.steps:step1. create db, create table.
         * @tc.expected: step1. return ok.
         */
        PrepareDataBase(g_tableName, true);

        /**
         * @tc.steps:step2. call GetLogInfoByPrimaryKeyOrGid with id = 2.
         * @tc.expected: step2. return E_NOT_FOUND.
         */
        std::shared_ptr<StorageProxy> storageProxy = GetStorageProxy(g_cloudStore);
        ASSERT_NE(storageProxy, nullptr);
        EXPECT_EQ(storageProxy->StartTransaction(), E_OK);
        VBucket vBucket;
        vBucket["id"] = 2L ;
        std::string gid = "";
        vBucket["GID_FIELD"] = gid;
        LogInfo logInfo;
        EXPECT_EQ(storageProxy->GetLogInfoByPrimaryKeyOrGid(g_tableName, vBucket, logInfo), -E_NOT_FOUND);

        /**
        * @tc.steps:step3. call GetLogInfoByPrimaryKeyOrGid with id = 1.
        * @tc.expected: step3. return E_OK.
        */
        vBucket["id"] = 1L;
        EXPECT_EQ(storageProxy->GetLogInfoByPrimaryKeyOrGid(g_tableName, vBucket, logInfo), E_OK);
    }
}
