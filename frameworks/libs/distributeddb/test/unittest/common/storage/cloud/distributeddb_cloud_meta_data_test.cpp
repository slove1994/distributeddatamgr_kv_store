/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "cloud/cloud_meta_data.h"

#include <gtest/gtest.h>

#include "db_errno.h"
#include "distributeddb_tools_unit_test.h"
#include "relational_store_manager.h"
#include "distributeddb_data_generate_unit_test.h"
#include "relational_sync_able_storage.h"
#include "relational_store_instance.h"
#include "sqlite_relational_store.h"
#include "log_table_manager_factory.h"

#define TABLE_NAME_1 "tableName1"
#define TABLE_NAME_2 "tableName2"

using namespace testing::ext;
using namespace DistributedDB;
using namespace DistributedDBUnitTest;
using namespace std;

namespace {
    const string g_storeID = "Relational_Store_ID";
    const string g_tableName = "cloudData";
    const string g_logTblName = DBConstant::RELATIONAL_PREFIX + g_tableName + "_log";
    const Timestamp g_startTime = 100000;
    const int g_flag = 0x02;
    string g_testDir;
    string g_storePath = "./g_store.db";
    DistributedDB::RelationalStoreManager g_mgr(APP_ID, USER_ID);
    RelationalStoreDelegate *g_delegate = nullptr;
    IRelationalStore *g_store = nullptr;
    std::shared_ptr<StorageProxy> g_storageProxy = nullptr;

    void CreateDB()
    {
        sqlite3 *db = nullptr;
        int errCode = sqlite3_open(g_storePath.c_str(), &db);
        if (errCode != SQLITE_OK) {
            LOGE("open db failed:%d", errCode);
            sqlite3_close(db);
            return;
        }

        const string sql =
            "PRAGMA journal_mode=WAL;";
        char *zErrMsg = nullptr;
        errCode = sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &zErrMsg);
        if (errCode != SQLITE_OK) {
            LOGE("sql error:%s", zErrMsg);
            sqlite3_free(zErrMsg);
        }
        sqlite3_close(db);
    }

    void CreateLogTable()
    {
        TableInfo table;
        table.SetTableName(g_tableName);
        table.SetTableSyncType(TableSyncType::CLOUD_COOPERATION);
        sqlite3 *db = nullptr;
        ASSERT_EQ(sqlite3_open(g_storePath.c_str(), &db), SQLITE_OK);
        auto tableManager =
            LogTableManagerFactory::GetTableManager(DistributedTableMode::COLLABORATION, TableSyncType::CLOUD_COOPERATION);
        int errCode = tableManager->CreateRelationalLogTable(db, table);
        EXPECT_EQ(errCode, E_OK);
        sqlite3_close(db);
    }

    void InitLogData(int64_t count, int flag)
    {
        sqlite3 *db = nullptr;
        ASSERT_EQ(sqlite3_open(g_storePath.c_str(), &db), SQLITE_OK);
        for (int i = 1; i <= count; ++i) {
            string sql = "INSERT OR REPLACE INTO " + g_logTblName
            + " (data_key, device, ori_device, Timestamp, wTimestamp, flag, hash_key, cloud_gid)"
            + " VALUES ('" + std::to_string(i) + "', '', '', '" +  std::to_string(g_startTime + i) + "', '"
            + std::to_string(g_startTime + i) + "','" + std::to_string(flag) + "','" + std::to_string(i) + "', '');";
            ASSERT_EQ(SQLiteUtils::ExecuteRawSQL(db, sql.c_str()), E_OK);
        }
        sqlite3_close(db);
    }

    void InitStoreProp(const std::string &storePath, const std::string &appId, const std::string &userId,
        RelationalDBProperties &properties)
    {
        properties.SetStringProp(RelationalDBProperties::DATA_DIR, storePath);
        properties.SetStringProp(RelationalDBProperties::APP_ID, appId);
        properties.SetStringProp(RelationalDBProperties::USER_ID, userId);
        properties.SetStringProp(RelationalDBProperties::STORE_ID, g_storeID);
        std::string identifier = userId + "-" + appId + "-" + g_storeID;
        std::string hashIdentifier = DBCommon::TransferHashString(identifier);
        properties.SetStringProp(RelationalDBProperties::IDENTIFIER_DATA, hashIdentifier);
    }

    const RelationalSyncAbleStorage *GetRelationalStore()
    {
        RelationalDBProperties properties;
        InitStoreProp(g_storePath, APP_ID, USER_ID, properties);
        int errCode = E_OK;
        g_store = RelationalStoreInstance::GetDataBase(properties, errCode);
        if (g_store == nullptr) {
            LOGE("Get db failed:%d", errCode);
            return nullptr;
        }
        return static_cast<SQLiteRelationalStore *>(g_store)->GetStorageEngine();
    }

    std::shared_ptr<StorageProxy> GetStorageProxy(ICloudSyncStorageInterface *store)
    {
        return StorageProxy::GetCloudDb(store);
    }

    void SetAndGetWaterMark(TableName tableName, LocalWaterMark mark)
    {
    LocalWaterMark retMark;
    EXPECT_EQ(g_storageProxy->PutLocalWaterMark(tableName, mark), E_OK);
    EXPECT_EQ(g_storageProxy->GetLocalWaterMark(tableName, retMark), E_OK);
    EXPECT_EQ(retMark, mark);
    }

    void SetAndGetWaterMark(TableName tableName, CloudWaterMark mark)
    {
    CloudWaterMark retMark;
    EXPECT_EQ(g_storageProxy->PutCloudWaterMark(tableName, mark), E_OK);
    EXPECT_EQ(g_storageProxy->GetCloudWaterMark(tableName, retMark), E_OK);
    EXPECT_EQ(retMark, mark);
    }
}

class DistributedDBCloudMetaDataTest : public testing::Test {
public:
    static void SetUpTestCase(void);
    static void TearDownTestCase(void);
    void SetUp();
    void TearDown();
};

void DistributedDBCloudMetaDataTest::SetUpTestCase(void)
{
}

void DistributedDBCloudMetaDataTest::TearDownTestCase(void)
{
}

void DistributedDBCloudMetaDataTest::SetUp(void)
{
    DistributedDBToolsUnitTest::PrintTestCaseInfo();
    LOGD("Test dir is %s", g_testDir.c_str());
    CreateDB();
    ASSERT_EQ(g_mgr.OpenStore(g_storePath, g_storeID, RelationalStoreDelegate::Option {}, g_delegate), DBStatus::OK);
    ASSERT_NE(g_delegate, nullptr);
    g_storageProxy = GetStorageProxy((ICloudSyncStorageInterface *) GetRelationalStore());
}

void DistributedDBCloudMetaDataTest::TearDown(void)
{
    if (g_delegate != nullptr) {
        EXPECT_EQ(g_mgr.CloseStore(g_delegate), DBStatus::OK);
        g_delegate = nullptr;
        g_storageProxy = nullptr;
    }
    if (DistributedDBToolsUnitTest::RemoveTestDbFiles(g_testDir) != 0) {
        LOGE("rm test db files error.");
    }
}

/**
  * @tc.name: CloudMetaDataTest001
  * @tc.desc: Set and get local water mark with various value
  * @tc.type: FUNC
  * @tc.require: 
  * @tc.author: wanyi
  */
HWTEST_F(DistributedDBCloudMetaDataTest, CloudMetaDataTest001, TestSize.Level0)
{
  SetAndGetWaterMark(TABLE_NAME_1, 123);
  SetAndGetWaterMark(TABLE_NAME_1, 0);
  SetAndGetWaterMark(TABLE_NAME_1, -1);
  SetAndGetWaterMark(TABLE_NAME_1, UINT64_MAX);
  SetAndGetWaterMark(TABLE_NAME_1, UINT64_MAX + 1);
}

/**
  * @tc.name: CloudMetaDataTest002
  * @tc.desc: Set and get cloud water mark with various value
  * @tc.type: FUNC
  * @tc.require: 
  * @tc.author: wanyi
  */
HWTEST_F(DistributedDBCloudMetaDataTest, CloudMetaDataTest002, TestSize.Level0)
{
  SetAndGetWaterMark(TABLE_NAME_1, "");
  SetAndGetWaterMark(TABLE_NAME_1, "123");
  SetAndGetWaterMark(TABLE_NAME_1, "1234567891012112345678910121");
  SetAndGetWaterMark(TABLE_NAME_1, "ABCDEFGABCDEFGABCDEFGABCDEFG");
  SetAndGetWaterMark(TABLE_NAME_1, "abcdefgabcdefgabcdefgabcdefg");
  SetAndGetWaterMark(TABLE_NAME_1, "ABCDEFGABCDEFGabcdefgabcdefg");
  SetAndGetWaterMark(TABLE_NAME_1, "123456_GABEFGab@中文字符cdefg");
}

/**
  * @tc.name: CloudMetaDataTest003
  * @tc.desc:
  * @tc.type: FUNC
  * @tc.require: 
  * @tc.author: wanyi
  */
HWTEST_F(DistributedDBCloudMetaDataTest, CloudMetaDataTest003, TestSize.Level0)
{
    CloudWaterMark retMark;
    EXPECT_EQ(g_storageProxy->GetCloudWaterMark(TABLE_NAME_2, retMark), E_OK);
    EXPECT_EQ(retMark, "");

    LocalWaterMark retLocalMark;
    EXPECT_EQ(g_storageProxy->GetLocalWaterMark(TABLE_NAME_2, retLocalMark), E_OK);
    EXPECT_EQ(retLocalMark, 0u);
}