/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "cloud/schema_mgr.h"

#include <gtest/gtest.h>

#include "db_errno.h"
#include "distributeddb_tools_unit_test.h"
#include "relational_store_manager.h"
#include "relational_schema_object.h"
#include "distributeddb_data_generate_unit_test.h"
#include "relational_sync_able_storage.h"
#include "relational_store_instance.h"
#include "sqlite_relational_store.h"
#include "log_table_manager_factory.h"

#define TABLE_NAME_1 "tableName1"
#define TABLE_NAME_2 "tableName2"
#define FIELD_NAME_1 "field_name_1"
#define FIELD_NAME_2 "field_name_2"
#define FIELD_NAME_3 "field_name_3"

using namespace testing::ext;
using namespace DistributedDB;
using namespace DistributedDBUnitTest;
using namespace std;

namespace {

    enum CloudTypeIdx {
        NULL_TYPE = 0,
        INT_TYPE = 1,
        DOUBLE_TYPE = 2,
        STRING_TYPE = 3,
        BOOL_TYPE = 4,
        BYTES_TYPE = 5,
        ASSET_TYPE = 6,
        ASSETS_TYPES = 7,
    };

  const string g_storeID = "Relational_Store_ID";
  const string g_tableName = "cloudData";
  const string g_logTblName = DBConstant::RELATIONAL_PREFIX + g_tableName + "_log";
  const Timestamp g_startTime = 100000;
  const int g_flag = 0x02;
  string g_testDir;
  string g_storePath = "./g_store.db";
  DistributedDB::RelationalStoreManager g_mgr(APP_ID, USER_ID);
  RelationalStoreDelegate *g_delegate = nullptr;
  IRelationalStore *g_store = nullptr;

  void CreateDB()
  {
      sqlite3 *db = nullptr;
      int errCode = sqlite3_open(g_storePath.c_str(), &db);
      if (errCode != SQLITE_OK) {
          LOGE("open db failed:%d", errCode);
          sqlite3_close(db);
          return;
      }

      const string sql =
          "PRAGMA journal_mode=WAL;";
      char *zErrMsg = nullptr;
      errCode = sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &zErrMsg);
      if (errCode != SQLITE_OK) {
          LOGE("sql error:%s", zErrMsg);
          sqlite3_free(zErrMsg);
      }
      sqlite3_close(db);
  }

  void CreateLogTable()
  {
      TableInfo table;
      table.SetTableName(g_tableName);
      table.SetTableSyncType(TableSyncType::CLOUD_COOPERATION);
      sqlite3 *db = nullptr;
      ASSERT_EQ(sqlite3_open(g_storePath.c_str(), &db), SQLITE_OK);
      auto tableManager =
          LogTableManagerFactory::GetTableManager(DistributedTableMode::COLLABORATION, TableSyncType::CLOUD_COOPERATION);
      int errCode = tableManager->CreateRelationalLogTable(db, table);
      EXPECT_EQ(errCode, E_OK);
      sqlite3_close(db);
  }

  void InitLogData(int64_t count, int flag)
  {
      sqlite3 *db = nullptr;
      ASSERT_EQ(sqlite3_open(g_storePath.c_str(), &db), SQLITE_OK);
      for (int i = 1; i <= count; ++i) {
          string sql = "INSERT OR REPLACE INTO " + g_logTblName
          + " (data_key, device, ori_device, Timestamp, wTimestamp, flag, hash_key, cloud_gid)"
          + " VALUES ('" + std::to_string(i) + "', '', '', '" +  std::to_string(g_startTime + i) + "', '"
          + std::to_string(g_startTime + i) + "','" + std::to_string(flag) + "','" + std::to_string(i) + "', '');";
          ASSERT_EQ(SQLiteUtils::ExecuteRawSQL(db, sql.c_str()), E_OK);
      }
      sqlite3_close(db);
  }

  void InitStoreProp(const std::string &storePath, const std::string &appId, const std::string &userId,
      RelationalDBProperties &properties)
  {
      properties.SetStringProp(RelationalDBProperties::DATA_DIR, storePath);
      properties.SetStringProp(RelationalDBProperties::APP_ID, appId);
      properties.SetStringProp(RelationalDBProperties::USER_ID, userId);
      properties.SetStringProp(RelationalDBProperties::STORE_ID, g_storeID);
      std::string identifier = userId + "-" + appId + "-" + g_storeID;
      std::string hashIdentifier = DBCommon::TransferHashString(identifier);
      properties.SetStringProp(RelationalDBProperties::IDENTIFIER_DATA, hashIdentifier);
  }

  const RelationalSyncAbleStorage *GetRelationalStore()
  {
      RelationalDBProperties properties;
      InitStoreProp(g_storePath, APP_ID, USER_ID, properties);
      int errCode = E_OK;
      g_store = RelationalStoreInstance::GetDataBase(properties, errCode);
      if (g_store == nullptr) {
          LOGE("Get db failed:%d", errCode);
          return nullptr;
      }
      
      return static_cast<SQLiteRelationalStore *>(g_store)->GetStorageEngine();
  }

  std::shared_ptr<StorageProxy> GetStorageProxy(ICloudSyncStorageInterface *store)
  {
      return StorageProxy::GetCloudDb(store);
  }

  std::shared_ptr<StorageProxy> g_storageProxy = nullptr;
  ICloudSyncStorageInterface *g_storeEngine = nullptr;
}

class DistributedDBSchemaMgrTest : public testing::Test {
public:
    static void SetUpTestCase(void);
    static void TearDownTestCase(void);
    void SetUp();
    void TearDown();
};

void DistributedDBSchemaMgrTest::SetUpTestCase(void)
{
}

void DistributedDBSchemaMgrTest::TearDownTestCase(void)
{
}

void DistributedDBSchemaMgrTest::SetUp(void)
{
  DistributedDBToolsUnitTest::PrintTestCaseInfo();
  LOGD("Test dir is %s", g_testDir.c_str());
  CreateDB();
  ASSERT_EQ(g_mgr.OpenStore(g_storePath, g_storeID, RelationalStoreDelegate::Option {}, g_delegate), DBStatus::OK);
  ASSERT_NE(g_delegate, nullptr);
  g_storeEngine = (ICloudSyncStorageInterface *) GetRelationalStore();
  g_storageProxy = GetStorageProxy(g_storeEngine);
}

void DistributedDBSchemaMgrTest::TearDown(void)
{
    if (g_delegate != nullptr) {
        EXPECT_EQ(g_mgr.CloseStore(g_delegate), DBStatus::OK);
        g_delegate = nullptr;
    }
    if (DistributedDBToolsUnitTest::RemoveTestDbFiles(g_testDir) != 0) {
        LOGE("rm test db files error.");
    }
    return;
}

DataBaseSchema g_schema = {
    .tables = {
        {
            .name = TABLE_NAME_2,
            .fields = {
                {
                    .colName = FIELD_NAME_2,
                    .type = INT_TYPE,
                    .primary = false,
                    .nullable = true,
                },
                {
                    .colName = FIELD_NAME_1,
                    .type = INT_TYPE,
                    .primary = true,
                    .nullable = true,
                }
            }
        }
    }
};

FieldInfo SetField(std::string fieldName, std::string dataType, std::string defaultVal)
{
    FieldInfo field;
    field.SetFieldName(fieldName);
    field.SetDataType(dataType);
    field.SetDefaultValue(defaultVal);
    return field;
}

/**
  * @tc.name: SchemaMgrTest001
  * @tc.desc: Cloud schema and local schema are not been set
  * @tc.type: FUNC
  * @tc.require: 
  * @tc.author: wanyi
  */
HWTEST_F(DistributedDBSchemaMgrTest, SchemaMgrTest001, TestSize.Level0)
{
    std::vector<TableName> tables = { g_tableName };
    EXPECT_EQ(g_storageProxy->CheckSchema(tables), -E_SCHEMA_NOT_SET);
}

/**
  * @tc.name: SchemaMgrTest002
  * @tc.desc: Cloud schema and local schema are the same
  * @tc.type: FUNC
  * @tc.require: 
  * @tc.author: wanyi
  */
HWTEST_F(DistributedDBSchemaMgrTest, SchemaMgrTest002, TestSize.Level0)
{
    FieldInfo field1 = SetField(FIELD_NAME_1, "int", "0");
    FieldInfo field2 = SetField(FIELD_NAME_2, "int", "0");
    TableInfo table;
    table.SetTableName(TABLE_NAME_2);
    table.AddField(field1);
    table.AddField(field2);
    table.SetPrimaryKey(FIELD_NAME_1, 1);
    RelationalSchemaObject localSchema;
    localSchema.AddRelationalTable(table);

    Key schemaKey;
    Value schemaVal;
    DBCommon::StringToVector(DBConstant::RELATIONAL_SCHEMA_KEY, schemaKey);
    DBCommon::StringToVector(localSchema.ToSchemaString(), schemaVal);
    int errCode = g_storeEngine->PutMetaData(schemaKey, schemaVal);

    EXPECT_EQ(g_storeEngine->SetCloudDbSchema(g_schema), E_OK);
    std::vector<TableName> tables = { TABLE_NAME_2 };
    EXPECT_EQ(g_storageProxy->CheckSchema(tables), E_OK);
    EXPECT_EQ(g_storageProxy->CheckSchema(tables), E_OK);
    EXPECT_EQ(g_storageProxy->CheckSchema(tables), E_OK);
}

/**
  * @tc.name: SchemaMgrTest003
  * @tc.desc: Local schema contain extra primary key
  * @tc.type: FUNC
  * @tc.require: 
  * @tc.author: wanyi
  */
HWTEST_F(DistributedDBSchemaMgrTest, SchemaMgrTest003, TestSize.Level0)
{
    FieldInfo field1 = SetField(FIELD_NAME_1, "int", "0");
    FieldInfo field2 = SetField(FIELD_NAME_2, "int", "0");
    FieldInfo field3 = SetField(FIELD_NAME_3, "int", "0");

    TableInfo table;
    table.SetTableName(TABLE_NAME_2);
    table.AddField(field1);
    table.AddField(field2);
    table.AddField(field3);
    table.SetPrimaryKey(FIELD_NAME_1, 1);
    table.SetPrimaryKey(FIELD_NAME_3, 2);
    RelationalSchemaObject localSchema;
    localSchema.AddRelationalTable(table);

    Key schemaKey;
    Value schemaVal;
    DBCommon::StringToVector(DBConstant::RELATIONAL_SCHEMA_KEY, schemaKey);
    DBCommon::StringToVector(localSchema.ToSchemaString(), schemaVal);
    int errCode = g_storeEngine->PutMetaData(schemaKey, schemaVal);

    EXPECT_EQ(g_storeEngine->SetCloudDbSchema(g_schema), E_OK);
    DataBaseSchema retCloudSchema;
    EXPECT_EQ(g_storeEngine->GetCloudDbSchema(retCloudSchema), E_OK);
    TableSchema retTableSchema;
    EXPECT_EQ(g_storeEngine->GetCloudTableSchema(TABLE_NAME_2, retTableSchema), E_OK);
    std::vector<TableName> tables = { TABLE_NAME_2 };
    EXPECT_EQ(g_storageProxy->CheckSchema(tables), -E_SCHEMA_MISMATCH);
}

/**
  * @tc.name: SchemaMgrTest003
  * @tc.desc: Column from local schema is not within cloud schema but is not nullable
  * @tc.type: FUNC
  * @tc.require: 
  * @tc.author: wanyi
  */
HWTEST_F(DistributedDBSchemaMgrTest, SchemaMgrTest004, TestSize.Level0)
{
    FieldInfo field1 = SetField(FIELD_NAME_1, "int", "0");
    FieldInfo field2 = SetField(FIELD_NAME_2, "int", "0");
    // field3 do not contain default value
    FieldInfo field3;
    field3.SetFieldName(FIELD_NAME_3);
    field3.SetDataType("int");

    TableInfo table;
    table.SetTableName(TABLE_NAME_2);
    table.AddField(field1);
    table.AddField(field2);
    table.AddField(field3);
    table.SetPrimaryKey(FIELD_NAME_1, 1);
    RelationalSchemaObject localSchema;
    localSchema.AddRelationalTable(table);

    Key schemaKey;
    Value schemaVal;
    DBCommon::StringToVector(DBConstant::RELATIONAL_SCHEMA_KEY, schemaKey);
    DBCommon::StringToVector(localSchema.ToSchemaString(), schemaVal);
    int errCode = g_storeEngine->PutMetaData(schemaKey, schemaVal);

    EXPECT_EQ(g_storeEngine->SetCloudDbSchema(g_schema), E_OK);
    std::vector<TableName> tables = { TABLE_NAME_2 };
    EXPECT_EQ(g_storageProxy->CheckSchema(tables), -E_SCHEMA_MISMATCH);
}